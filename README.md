# Golang Devcontainer

Golang Devcontainer image for Visual Studio Code.

## Commit messages

Commit messages follow the [Gitmoji](https://gitmoji.carloscuesta.me/) pattern.