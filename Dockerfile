FROM golang:1.15.0-buster

RUN apt-get update

WORKDIR $GOPATH/src

RUN apt-get install -y  git curl tzdata \
    && mkdir -p /tmp/gotools \
    && cd /tmp/gotools \
    && GO111MODULE=on go get -v golang.org/x/tools/gopls@latest 2>&1 \
    && GO111MODULE=on go get -v \
    honnef.co/go/tools/...@latest \
    golang.org/x/tools/cmd/gorename@latest \
    golang.org/x/tools/cmd/goimports@latest \
    golang.org/x/tools/cmd/guru@latest \
    golang.org/x/lint/golint@latest \
    github.com/mdempsky/gocode@latest \
    github.com/cweill/gotests/...@latest \
    github.com/haya14busa/goplay/cmd/goplay@latest \
    github.com/sqs/goreturns@latest \
    github.com/josharian/impl@latest \
    github.com/davidrjenni/reftools/cmd/fillstruct@latest \
    github.com/uudashr/gopkgs/v2/cmd/gopkgs@latest \
    github.com/ramya-rao-a/go-outline@latest  \
    github.com/acroca/go-symbols@latest  \
    github.com/godoctor/godoctor@latest  \
    github.com/rogpeppe/godef@latest  \
    github.com/zmb3/gogetdoc@latest \
    github.com/fatih/gomodifytags@latest  \
    github.com/mgechev/revive@latest  \
    github.com/go-delve/delve/cmd/dlv@latest 2>&1 \
    && go get -v github.com/alecthomas/gometalinter 2>&1 \
    && go get -x -d github.com/stamblerre/gocode 2>&1 \
    && go build -o gocode-gomod github.com/stamblerre/gocode \
    && mv gocode-gomod $GOPATH/bin/ \
    && curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b $(go env GOPATH)/bin 2>&1 \
    && chmod -R a+w /go/pkg

RUN git config --global url."git@gitlab.com:".insteadOf "https://gitlab.com/"
